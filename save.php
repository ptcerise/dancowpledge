<?php
/**
 * Created by PhpStorm.
 * User: Harliandi
 * Date: 5/2/2017
 * Time: 1:42 PM
 */

$email = $id = $id_folder = $age = "";

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["id"])) {
    $id = check($_POST["id"]);
    $email = check($_POST["email"]);
    $age = check($_POST["age"]);
    create_edit_user($email, $id, $age);
} else {
    if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST["id_folder"])) {
        $id_folder = check($_POST["id_folder"]);
        get_folder_detail($id_folder);
    } else {
        $email = check($_POST["email"]);
        get_user($email);
    }
}

/**
 * Sanitize the input
 * @param $data
 * @return string
 */
function check($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}

/**
 * Get user data
 * @param $email
 */
function get_user($email)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.sendinblue.com/v2.0/user/" . $email);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    $headers = array();
    $headers[] = "api-key:DOaTBIfWyS4Lbh0n";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    echo $result;
}

/**
 * Get folder detail
 * @param $id
 */
function get_folder_detail($id)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.sendinblue.com/v2.0/folder/" . $id);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    $headers = array();
    $headers[] = "Api-Key: DOaTBIfWyS4Lbh0n";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    echo $result;
}

/**
 * Create or Update user data
 * @param $email
 * @param $id
 * @param $fname
 * @param $lname
 * @param $phone
 * @param $birthday
 */
function create_edit_user($email, $id, $age)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, "https://api.sendinblue.com/v2.0/user/createdituser");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,
        "{\"email\":\"" . $email . "\",\"attributes\":{\"FACEBOOK_ID\":\"" . $id . "\",\"UMUR_ANAK\":\"" . $age . "\"},\"listid\":[46]}");
    curl_setopt($ch, CURLOPT_POST, 1);

    $headers = array();
    $headers[] = "api-key:DOaTBIfWyS4Lbh0n";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        echo 'Error:' . curl_error($ch);
    }
    curl_close($ch);
    echo $result;
}
